#!/bin/bash
set -e

# Run composer installation on start up
cd /var/www/

echo "--------- Install composer dependencies"

    composer update symfony/flex --no-plugins --no-scripts &&
    composer install --prefer-dist --no-scripts --no-progress &&
    composer clear-cache &&
    composer dump-autoload

  php bin/console doctrine:database:create --if-not-exists
  echo "Run database create."

  # Run Doctrine migrations
  php bin/console doctrine:migrations:migrate --no-interaction
  echo "Run doctrine migrations."


  #configure Jwt
  php bin/console lexik:jwt:generate-keypair --overwrite

echo "--------- Ready!"
exec "$@"