# EveryPayInterview



## Description

This Project is a dockerized application that serves only one purpose. Create
a payment charge using a dynamic Payment Gateway depending on user. It is serving as a proxy between merchants and Payment Service Providers 
like Stripe or PinPayments. It accepts a payload of card credentials and ammount, and according to the merchant settings, it will
charge the payment card to the respective PSP.

## Getting Started

It requires only few commands to get things up and running.
The only requirement is a docker deamon to be installed. In my case it was [Docker Desktop for Windows](https://www.docker.com/products/docker-desktop/) that provides a docker deamon among other docker services.

Firstly, we need to clone the git repository to our local machine. Navigate to a directory of your preference and type:

```
git clone https://gitlab.com/myprojects3693802/EveryPayInterview.git
```

After the cloning is completed, we should
- create  .env file based on .env.dist in root Directory. Fill in the APP_SECRET and JWT_PASSPHRASE with anything we wish. We should also change the username:pass
  DATABASE_URL with the credentials from database container located in docker-compose.yml.
- create a xdebug.ini file based on xdebug.ini.example in docker/php Directory.
- make sure docker/entrypoints/docker-entrypoint.sh has the correct Line Separator. LF for linux , CRLF for windows, CR for Mac distributions.

To run the application, navigate in project directory root and type:



```
docker-compose build

docker-compose up -d
```

- If it is the first time building the images, also run :

```
docker exec -it everypayinterview-php-1 php bin/console doctrine:fixtures:load --no-interaction --append

```
That's it! Everything else is taken care of in the background!

This will create 3 containers, including one for php, one for a nginx webserver and one for a postgres Database. It will also
populate the database with some dummy tables and values in order to be functional.





## Project Implementation && Details

This project is implemented with php 7.4 and Symfony framework v5.4. 
It is designed based on Ports and Adapters pattern, which helps to keep the base code (business logic code) clean and independent of external dependencies. 
Keeping the base code clean means, that Symfony framework is only used in concepts outside of project scope like authentication,database interaction, configuration files etc.

- **Authentication**

As mentioned before, for authentication i decided to go symfony/security bundle which provides a very flexible and easy way to implement authentication to your application.
I have stored in memory exactly two users with hardcoded credentials for simplicity’s sake. The user has to "login" with username/password to the application first in order to retrieve 
an authorization token, which is necessary to be sent alongside with data in each request.

- **Authorization**

The authorization is handled by symfony using the authorization token generated in authentication process. For the token generation and validation I have used 
lexik/LexikJWTAuthenticationBundle. I have configured all routes except the login route to be accessible only to users with valid authorization token.

- **Database Interaction**

Database interaction is happening with the help of Doctrine/ORM. This helps with the mapping of an object with a corresponding table in database. The mapping configuration
is happening with xml to keep the business entities clean from orm specific annotations, and therefore agnostic to the mapping implementation. 

- **Core**

In the heart of the application resides the application code. This part of code is agnostic to external dependencies and libraries, as it only implements Interfaces
( for external services ) and services that exists in the application core. Let's analyze it with an example.

When a user makes a request with card credentials and ammount, these requestData are mapped to an DTO that our application understands.
If the input data contains any null field it will result in error. This DTO is then being forwarded
throw an Interface to the core application. In this case the controller (which accepts the request) is the Driving Adapter and the interface is the Port to the application code.
The service that is implementing this Interface is a use case, with only purpose to use a paymentGateway with the DTO and make a charge. Again, it uses an interface
to exit the application core and forward the request to a concrete paymentGateway implementation. In this case the interface is the Port and the concrete implementation
is the Driven Adapter.

The decision of the concrete paymentGateway implementation is taken by a Factory service. There, by using core services and interfaces, it retrieves the username 
from authorisation token and perform a search query to database to retrieve client credentials and psp preference based on username. Based on the return value, 
it returns the correct implementation of paymentGatewayInterface according to user settings.

- **Error Handling**

When an error is caused by the client side, the application will return corresponding message indicating the error. Examples are wrong credentials,expired token,wrong input data.
For errors that are occurred during application runtime the application will return a message with success = false and 500 Error. The actual error will
be logged to a file in /var/log with name the current date. Each day, a new file will be created keeping only the last 5 days. 
For logging, I used symfony/monolog-bundle.

## Api Endpoints

This api provides only two endpoints

1) ```/api/auth [POST]```

This is used for obtaining the authorization token. It requires a body with json data containing username and password. You can use one of provided fo test purposes.

```
{
"username" : "nakosswimwear@testmail.com",
"password" : "testpass2"
}

{
"username" : "nakosfootwear@testmail.com",
"password" : "testpass1"
}
```

2) ```/api/payment [POST]```

before sending the request, make sure you set the Authorization to Bearer Token and use the token returned from auth path.

You cant test it with this example data

```
{
    "card_number" : "1245684151714",
    "expiration_date" : "12/28",
    "cvv" : "123",
    "cardholder_name" : "Interview Test",
    "ammount" : "5000"
}
```

Make sure to not leave any field blank as this will result in Bad Request. It is a simple validation rule used for demonstration purposes.

If everything works, you should see a ```success => true```  with a ChargeID relevant message.

Enjoy!