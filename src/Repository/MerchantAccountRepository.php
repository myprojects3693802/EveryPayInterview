<?php

namespace App\Repository;

use App\Application\Entity\MerchantAccount;
use App\Application\Interfaces\MerchantAccountRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class MerchantAccountRepository implements MerchantAccountRepositoryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getMerchantByUsername(string $username): ?MerchantAccount
    {
        return $this->entityManager->getRepository(MerchantAccount::class)->findOneBy(['username' => $username]);
    }
}