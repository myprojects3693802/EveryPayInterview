<?php

declare(strict_types=1);

namespace App\Controller;

use App\Application\DTO\ChargeCardDetailsDTO;
use App\Application\DTO\Exceptions\InputValidationException;
use App\Application\Interfaces\PaymentInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PaymentController extends AbstractController
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/api/payment", name="app_payment", methods={"POST"})
     */
    public function chargePayment(
        PaymentInterface $payment,
        ChargeCardDetailsDTO $chargeCardDetailsDTO
    ): JsonResponse {
        try {
            $chargeCardDetailsDTO->populate();

            return $this->json([
                'success' => 'true',
                'message' => $payment->chargePaymentCard($chargeCardDetailsDTO)
            ]);
        } catch (InputValidationException $e) {
            return $this->json($e->getMessage(), Response::HTTP_BAD_REQUEST);
        } catch (\Throwable $exception) {
            $this->logger->alert(
                'Exception: ' . $exception->getMessage(),
                ["exception Trace" => $exception]
            );
            return $this->json([
                'success' => 'false'
            ],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }
}
