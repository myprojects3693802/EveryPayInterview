<?php

namespace App\Plugins\PinPayments;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class PinPaymentsClientService

{
    public HttpClientInterface $httpClient;

    public string $baseUrl;

    public string $apiKey;

    public function __construct(string $apikey)
    {
        $this->apiKey = $apikey;
        $this->baseUrl = 'https://test-api.pinpayments.com';
    }


    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     */
    public function createCharge(array $data): array
    {
        $this->setUpClient();
        $url = $this->baseUrl . '/1/charges';

        $response = $this->httpClient->request('POST', $url, [
            'auth_basic' => $this->apiKey . ':',
            'json' => $data,
        ]);

        return $response->toArray();
    }

    public function setUpClient()
    {
        $this->httpClient = HttpClient::create();
    }
}