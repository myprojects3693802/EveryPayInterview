<?php

namespace App\Application\Entity;


class MerchantAccount
{
    private $id;
    private ?string $username;
    private ?string $password;
    private ?string $merchantWebsite;
    private ?string $pspPreference;
    private ?string $pspAuth;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getMerchantWebsite(): ?string
    {
        return $this->merchantWebsite;
    }

    public function setMerchantWebsite(string $merchantWebsite): void
    {
        $this->merchantWebsite = $merchantWebsite;
    }

    public function getPspPreference(): ?string
    {
        return $this->pspPreference;
    }

    public function setPspPreference(string $pspPreference): void
    {
        $this->pspPreference = $pspPreference;
    }

    public function getPspAuth(): ?string
    {
        return $this->pspAuth;
    }

    public function setPspAuth(string $psrAuth): void
    {
        $this->pspAuth = $psrAuth;
    }

}