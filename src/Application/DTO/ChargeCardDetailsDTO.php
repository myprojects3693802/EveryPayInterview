<?php
declare(strict_types=1);

namespace App\Application\DTO;

use App\Application\Services\BaseRequestObjectService;

class ChargeCardDetailsDTO extends BaseRequestObjectService
{
    public string $card_number;

    public string $expiration_date;

    public string $cvv;

    public string $cardholder_name;

    public string $ammount;

    public function map(\stdClass $inputObject)
    {
        return $this->jsonMapping
            ->mapToClass($inputObject, self::class);
    }


}