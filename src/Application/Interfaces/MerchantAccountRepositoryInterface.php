<?php

namespace App\Application\Interfaces;

use App\Application\Entity\MerchantAccount;

interface MerchantAccountRepositoryInterface
{
    public function getMerchantByUsername(string $username): ?MerchantAccount;
}