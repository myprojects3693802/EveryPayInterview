<?php

namespace App\Application\Interfaces;

use Symfony\Component\HttpFoundation\Request;

interface RequestInterface
{
    public function getRequest(): ?Request;
}