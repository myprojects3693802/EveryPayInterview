<?php

namespace App\Application\Interfaces;

use App\Application\DTO\ChargeCardDetailsDTO;

interface PaymentInterface
{
    public function chargePaymentCard(ChargeCardDetailsDTO $chargeCardDetailsDTO);
}