<?php

namespace App\Application\Interfaces;

use App\Application\DTO\ChargeCardDetailsDTO;

interface PaymentGatewayInterface
{
    public function chargeCard(ChargeCardDetailsDTO $chargeCardDetailsDTO): string;
}