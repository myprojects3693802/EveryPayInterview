<?php

declare(strict_types=1);

namespace App\Application\Services;

use App\Application\DTO\Exceptions\InputValidationException;
use App\Application\Interfaces\RequestInterface;
use Exception;
use JsonMapper\JsonMapperInterface;
use Psr\Log\InvalidArgumentException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class BaseRequestObjectService
{
    protected JsonMapperInterface $jsonMapping;

    private RequestInterface $requestStack;

    private ValidatorInterface $validator;

    public function __construct(
        JsonMapperInterface $jsonMapping,
        RequestInterface $request,
        ValidatorInterface $validator
    ) {
        $this->jsonMapping = $jsonMapping;
        $this->requestStack = $request;
        $this->validator = $validator;
    }

    /**
     * @throws Exception
     */
    public function populate(): BaseRequestObjectService
    {
        $mappedObject = $this->map($this->getInputObject());

        foreach ((array)$mappedObject as $propertyName => $value) {
            if (property_exists($this, $propertyName)) {
                $this->{$propertyName} = $value;
            }
        }

        $this->validate();

        return $this;
    }

    abstract protected function map(\stdClass $inputObject);

    private function getInputObject(): ?\stdClass
    {
        $request = $this->requestStack->getRequest();
        return json_decode($request->getContent());
    }

    /**
     * @throws Exception
     */
    private function validate(): void
    {
        $errors = $this->validator->validate($this);
        if (count($errors) > 0) {
            throw new InputValidationException('Invalid input');
        }
    }
}