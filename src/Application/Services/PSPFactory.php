<?php

declare(strict_types=1);

namespace App\Application\Services;

use App\Services\PaymentGateway\PinPaymentGateway;
use App\Services\PaymentGateway\StripePaymentGateway;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Psr\Log\InvalidArgumentException;

class PSPFactory
{
    private PSPDeterminationService $pspDeterminationService;

    public function __construct(PSPDeterminationService $pspDeterminationService)
    {
        $this->pspDeterminationService = $pspDeterminationService;
    }

    /**
     * @throws JWTDecodeFailureException
     */
    public function getPaymentGatewayService()
    {
        [$merchantPsp, $pspCredentials] = $this->pspDeterminationService->getPSP();
        switch ($merchantPsp) {
            case 'Stripe':
                return new StripePaymentGateway($pspCredentials);
            case 'PinPayments':
                return new PinPaymentGateway($pspCredentials);
            default:
                throw new InvalidArgumentException('No defined PSP.');
        }
    }
}