<?php

declare(strict_types=1);

namespace App\Application\Services;

use App\Application\Interfaces\MerchantAccountRepositoryInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;

class PSPDeterminationService
{
    private AuthService $authService;
    private MerchantAccountRepositoryInterface $merchantAccountRepository;

    public function __construct(AuthService $authService, MerchantAccountRepositoryInterface $merchantAccountRepository)
    {
        $this->merchantAccountRepository = $merchantAccountRepository;
        $this->authService = $authService;
    }

    /**
     * @throws JWTDecodeFailureException
     */
    public function getPSP(): ?array
    {
        $username = $this->authService->extractUsernameFromToken();

        if (!empty($username)) {
            $merchantAccount = $this->merchantAccountRepository->getMerchantByUsername($username);

            return $merchantAccount ? [$merchantAccount->getPspPreference(), $merchantAccount->getPspAuth()] : null;

        }

        return null;
    }
}