<?php

declare(strict_types=1);

namespace App\Application\Services;

use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AuthService
{
    private JWTTokenManagerInterface $tokenManager;

    private TokenStorageInterface $tokenStorage;

    public function __construct(
        JWTTokenManagerInterface $tokenManager,
        TokenStorageInterface $tokenStorage
    ) {
        $this->tokenManager = $tokenManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @throws JWTDecodeFailureException
     */
    public function extractUsernameFromToken(): ?string
    {
        $token = $this->tokenStorage->getToken();

        if ($token !== null) {

            $tokenPayload = $this->tokenManager->decode($token);

            if (isset($tokenPayload['username'])) {
                return $tokenPayload['username'];
            }
        }

        return null;
    }
}