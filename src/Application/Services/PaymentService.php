<?php

namespace App\Application\Services;

use App\Application\DTO\ChargeCardDetailsDTO;
use App\Application\Interfaces\PaymentGatewayInterface;
use App\Application\Interfaces\PaymentInterface;

class PaymentService implements PaymentInterface
{
    public PaymentGatewayInterface $paymentGateway;

    public function __construct(PaymentGatewayInterface $paymentGateway)
    {
        $this->paymentGateway = $paymentGateway;
    }

    public function chargePaymentCard(ChargeCardDetailsDTO $chargeCardDetailsDTO): string
    {
        $chargeID = $this->paymentGateway->chargeCard($chargeCardDetailsDTO);
        return "Charge successful. Charge ID: " . $chargeID;
    }

}