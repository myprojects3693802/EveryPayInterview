<?php

declare(strict_types=1);

namespace App\Services\PaymentGateway;

use App\Application\DTO\ChargeCardDetailsDTO;
use App\Application\Interfaces\PaymentGatewayInterface;
use Stripe\Exception\ApiErrorException;
use Stripe\StripeClient;
use Stripe\StripeClientInterface;

class StripePaymentGateway implements PaymentGatewayInterface
{

    public StripeClientInterface $stripeClient;
    private string $apiKey;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @throws ApiErrorException
     */
    public function chargeCard(ChargeCardDetailsDTO $chargeCardDetailsDTO): string
    {
        $this->setUpClient();
        $charge = $this->stripeClient->charges->create(
            [
                'amount' => $chargeCardDetailsDTO->ammount,
                'currency' => 'usd',
                'source' => 'tok_visa',
                'description' => 'Test Charge'
            ]
        );

        return $charge->id;
    }

    public function setUpClient(): void
    {
        $this->stripeClient = new StripeClient(
            ['api_key' => $this->apiKey]
        );
    }
}