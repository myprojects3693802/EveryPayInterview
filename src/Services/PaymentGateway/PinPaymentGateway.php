<?php

declare(strict_types=1);

namespace App\Services\PaymentGateway;

use App\Application\DTO\ChargeCardDetailsDTO;
use App\Application\Interfaces\PaymentGatewayInterface;
use App\Plugins\PinPayments\PinPaymentsClientService;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class PinPaymentGateway implements PaymentGatewayInterface
{

    public PinPaymentsClientService $pinPaymentsClient;

    public string $apiKey;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;

    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function chargeCard(ChargeCardDetailsDTO $chargeCardDetailsDTO): string
    {
        $this->setUpClient();
        $response = $this->pinPaymentsClient->createCharge(
            [
                'email' => 'customer@example.com',
                'description' => 'test charge',
                'amount' => $chargeCardDetailsDTO->ammount,
                'ip_address' => '192.168.0.1',
                'card' => [
                    'number' => '3528800000000000',
                    'expiry_month' => substr($chargeCardDetailsDTO->expiration_date, 0, 2),
                    'expiry_year' => '20' . substr($chargeCardDetailsDTO->expiration_date, -2),
                    'cvc' => $chargeCardDetailsDTO->cvv,
                    'name' => $chargeCardDetailsDTO->cardholder_name,
                    'address_line1' => '42 Sevenoaks St',
                    'address_city' => 'Lathlain',
                    'address_postcode' => '6454',
                    'address_state' => 'WA',
                    'address_country' => 'Australia'
                ],

            ]
        );

        return $response['response']['token'];
    }

    public function setUpClient(): void
    {
        $this->pinPaymentsClient = new PinPaymentsClientService($this->apiKey);
    }
}