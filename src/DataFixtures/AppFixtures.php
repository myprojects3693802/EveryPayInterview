<?php

namespace App\DataFixtures;

use App\Application\Entity\MerchantAccount;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
         $merchantAccount1 = new MerchantAccount();
         $merchantAccount1->setUsername('nakosfootwear@testmail.com');
         $merchantAccount1->setPassword('$2y$10$CvQ4eQJVUr1tcauSPZeiPODUJPzvBCJjguzzWQnU3Uoaq4JgVw6Ha');
         $merchantAccount1->setMerchantWebsite('footwear.com');
         $merchantAccount1->setPspPreference('Stripe');
         $merchantAccount1->setPspAuth('sk_test_51OVWu1F794BSzjVeAF6LuiVJk5MJOfGVfed3P92eQuhw55sIN1ERJGboMh68dLzs0ZYglhMfGNmu5jM0kHG6cmHB00aqxZRRqh');

         $manager->persist($merchantAccount1);

        $merchantAccount2 = new MerchantAccount();
        $merchantAccount2->setUsername('nakosswimwear@testmail.com');
        $merchantAccount2->setPassword('$2y$10$QTYPfHfdZ9b4D/pKzLQxAO2U3xdlt5FLp0TSqZ3X.QY9rZ9oDA3NG');
        $merchantAccount2->setMerchantWebsite('swimwear.com');
        $merchantAccount2->setPspPreference('PinPayments');
        $merchantAccount2->setPspAuth('MKkaK9S47vdTlau9EycwYA');

        $manager->persist($merchantAccount2);
        $manager->flush();
    }
}
