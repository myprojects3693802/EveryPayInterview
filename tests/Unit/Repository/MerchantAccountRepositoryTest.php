<?php

namespace App\Tests\Unit\Repository;

use App\Application\Entity\MerchantAccount;
use App\Repository\MerchantAccountRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\TestCase;

class MerchantAccountRepositoryTest extends TestCase
{
    public function testGetMerchantByUsername(): void
    {
        $entityManagerMock = $this->createMock(EntityManagerInterface::class);

        $merchantAccount = new MerchantAccount();
        $merchantAccount->setUsername('testusername');

        $entityRepositoryMock = $this->createMock(EntityRepository::class);
        $entityRepositoryMock->method('findOneBy')->willReturn($merchantAccount);

        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->with(MerchantAccount::class)
            ->willReturn($entityRepositoryMock);

        $merchantAccountRepository = new MerchantAccountRepository($entityManagerMock);

        $result = $merchantAccountRepository->getMerchantByUsername('testusername');

        $this->assertInstanceOf(MerchantAccount::class, $result);

        $this->assertEquals('testusername', $result->getUsername());
    }

    public function testGetMerchantByUsernameWillReturnNull(): void
    {
        $entityManagerMock = $this->createMock(EntityManagerInterface::class);


        $entityRepositoryMock = $this->createMock(EntityRepository::class);
        $entityRepositoryMock->method('findOneBy')->willReturn(null);

        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->with(MerchantAccount::class)
            ->willReturn($entityRepositoryMock);

        $merchantAccountRepository = new MerchantAccountRepository($entityManagerMock);

        $result = $merchantAccountRepository->getMerchantByUsername('testusername');

        $this->assertNull($result);
    }
}
