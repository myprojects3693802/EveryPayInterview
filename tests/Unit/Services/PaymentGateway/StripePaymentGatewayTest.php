<?php

namespace App\Tests\Unit\Services\PaymentGateway;

use App\Application\DTO\ChargeCardDetailsDTO;
use App\Services\PaymentGateway\StripePaymentGateway;
use PHPUnit\Framework\TestCase;
use Stripe\Exception\ApiErrorException;
use Stripe\Service\ChargeService;
use Stripe\StripeClient;

class StripePaymentGatewayTest extends TestCase
{
    /**
     * @throws ApiErrorException
     */
    public function testChargePaymentCard()
    {
        $mockChargeCardDetailsDTO = $this->createMock(ChargeCardDetailsDTO::class);

        $stripeClientMock = $this->createMock(StripeClient::class);

        $mockChargeID = 'ch_123456789';

        $stripeClientMock->charges = $this->createMock(ChargeService::class);

        $stripeClientMock->charges->expects($this->once())
            ->method('create')
            ->willReturn((object)['id' => $mockChargeID]);

        $stripePaymentGateway= $this->getMockBuilder(StripePaymentGateway::class)
            ->setConstructorArgs(['testApiKey'])
            ->onlyMethods(['setUpClient'])
            ->getMock();

        $stripePaymentGateway->stripeClient = $stripeClientMock;

        $mockChargeCardDetailsDTO->ammount = 2000;

        $result = $stripePaymentGateway->chargeCard($mockChargeCardDetailsDTO);

        $this->assertEquals($mockChargeID, $result);
    }

    public function testChargePaymentCardWillThrowException()
    {
        $mockChargeCardDetailsDTO = $this->createMock(ChargeCardDetailsDTO::class);

        $stripeClientMock = $this->createMock(StripeClient::class);

        $stripeClientMock->charges = $this->createMock(ChargeService::class);

        $stripeClientMock->charges->expects($this->once())
            ->method('create')
            ->willThrowException($this->createMock(ApiErrorException::class));

        $stripePaymentGateway= $this->getMockBuilder(StripePaymentGateway::class)
            ->setConstructorArgs(['testApiKey'])
            ->onlyMethods(['setUpClient'])
            ->getMock();

        $stripePaymentGateway->stripeClient = $stripeClientMock;


        $mockChargeCardDetailsDTO->ammount = 2000;
        $this->expectException(ApiErrorException::class);

        $stripePaymentGateway->chargeCard($mockChargeCardDetailsDTO);
    }
}
