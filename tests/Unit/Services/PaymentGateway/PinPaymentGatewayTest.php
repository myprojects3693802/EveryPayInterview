<?php

namespace App\Tests\Unit\Services\PaymentGateway;

use App\Application\DTO\ChargeCardDetailsDTO;
use App\Plugins\PinPayments\PinPaymentsClientService;
use App\Services\PaymentGateway\PinPaymentGateway;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class PinPaymentGatewayTest extends TestCase
{

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testChargeCard()
    {
        $mockResponse = [
            "response" => [
                "token" => "testToken",
                "number" => "2",
                "ammountCharged" => "100"
            ]
        ];

        //set up values to bypass ::$ammount must not be accessed before initialization
        $mockChargeCardDetailsDTO = $this->createMock(ChargeCardDetailsDTO::class);
        $mockChargeCardDetailsDTO->ammount = 5000;
        $mockChargeCardDetailsDTO->expiration_date = '1223';
        $mockChargeCardDetailsDTO->cvv = '123';
        $mockChargeCardDetailsDTO->cardholder_name = 'aris nakos';


        $mockPinPaymentClient = $this->createMock(PinPaymentsClientService::class);

        $mockPinPaymentClient->method('createCharge')->willReturn($mockResponse);

        $pinPaymentGateway = $this->getMockBuilder(PinPaymentGateway::class)
            ->setConstructorArgs(["testApiKey"])
            ->onlyMethods(['setUpClient'])
            ->getMock();

        $pinPaymentGateway->pinPaymentsClient = $mockPinPaymentClient;


        $result = $pinPaymentGateway->chargeCard($mockChargeCardDetailsDTO);
        $this->assertEquals("testToken", $result);
    }

    public function testChargeCardWillThrowException()
    {

        //set up values to bypass ::$ammount must not be accessed before initialization
        $mockChargeCardDetailsDTO = $this->createMock(ChargeCardDetailsDTO::class);
        $mockChargeCardDetailsDTO->ammount = 5000;
        $mockChargeCardDetailsDTO->expiration_date = '1223';
        $mockChargeCardDetailsDTO->cvv = '123';
        $mockChargeCardDetailsDTO->cardholder_name = 'aris nakos';


        $mockPinPaymentClient = $this->createMock(PinPaymentsClientService::class);

        $mockPinPaymentClient->method('createCharge')->willThrowException($this->createMock(\Exception::class));

        $pinPaymentGateway = $this->getMockBuilder(PinPaymentGateway::class)
            ->setConstructorArgs(["testApiKey"])
            ->onlyMethods(['setUpClient'])
            ->getMock();

        $pinPaymentGateway->pinPaymentsClient = $mockPinPaymentClient;

        $this->expectException(\Exception::class);

        $result = $pinPaymentGateway->chargeCard($mockChargeCardDetailsDTO);
    }
}
