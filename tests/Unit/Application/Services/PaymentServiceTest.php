<?php

namespace App\Tests\Unit\Application\Services;

use App\Application\DTO\ChargeCardDetailsDTO;
use App\Application\Interfaces\PaymentGatewayInterface;
use App\Application\Services\PaymentService;
use App\Services\PaymentGateway\StripePaymentGateway;
use PHPUnit\Framework\TestCase;

class PaymentServiceTest extends TestCase
{
    public function testChargePaymentCard()
    {
        $mockPaymentGateway = $this->createMock(StripePaymentGateway::class);


        $mockChargeID = 'ch_3OVZPkF794BSzjVe0NVBdV6f';
        $mockPaymentGateway->method('chargeCard')
            ->willReturn($mockChargeID);


        $paymentService = new PaymentService($mockPaymentGateway);


        $mockChargeCardDetailsDTO = $this->createMock(ChargeCardDetailsDTO::class);


        $result = $paymentService->chargePaymentCard($mockChargeCardDetailsDTO);


        $expectedResult = "Charge successful. Charge ID: $mockChargeID";
        $this->assertEquals($expectedResult, $result);
    }
}
