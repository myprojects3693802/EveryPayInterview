<?php

namespace App\Tests\Unit\Application\Services;

use App\Application\Entity\MerchantAccount;
use App\Application\Interfaces\MerchantAccountRepositoryInterface;
use App\Application\Services\AuthService;
use App\Application\Services\PSPDeterminationService;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use PHPUnit\Framework\TestCase;

class PSPDeterminationServiceTest extends TestCase
{

    /**
     * @throws JWTDecodeFailureException
     */
    public function testGetPSPReturnsValidPSPData(): void
    {
        $authServiceMock = $this->createMock(AuthService::class);
        $authServiceMock->method('extractUsernameFromToken')->willReturn('test_username');

        $mockMerchantAccount = new MerchantAccount();
        $mockMerchantAccount->setPspPreference('Stripe');
        $mockMerchantAccount->setPspAuth('stripe_credentials');

        $merchantAccountRepositoryMock = $this->createMock(MerchantAccountRepositoryInterface::class);
        $merchantAccountRepositoryMock
            ->method('getMerchantByUsername')
            ->with('test_username')
            ->willReturn($mockMerchantAccount);

        $pspDeterminationService = new PSPDeterminationService($authServiceMock, $merchantAccountRepositoryMock);

        $result = $pspDeterminationService->getPSP();

        $this->assertEquals(['Stripe', 'stripe_credentials'], $result);
    }

    /**
     * @throws JWTDecodeFailureException
     */
    public function testGetPSPReturnsNullForEmptyUsername(): void
    {
        $authServiceMock = $this->createMock(AuthService::class);
        $authServiceMock->method('extractUsernameFromToken')->willReturn('');

        $merchantAccountRepositoryMock = $this->createMock(MerchantAccountRepositoryInterface::class);

        $pspDeterminationService = new PSPDeterminationService($authServiceMock, $merchantAccountRepositoryMock);

        $result = $pspDeterminationService->getPSP();

        $this->assertNull($result);
    }

    public function testGetPSPReturnsNullForNonexistentMerchantAccount(): void
    {
        $authServiceMock = $this->createMock(AuthService::class);
        $authServiceMock->method('extractUsernameFromToken')->willReturn('nonexistent_user');

        $merchantAccountRepositoryMock = $this->createMock(MerchantAccountRepositoryInterface::class);
        $merchantAccountRepositoryMock->method('getMerchantByUsername')->willReturn(null);

        $pspDeterminationService = new PSPDeterminationService($authServiceMock, $merchantAccountRepositoryMock);

        $result = $pspDeterminationService->getPSP();

        $this->assertNull($result);
    }

}
