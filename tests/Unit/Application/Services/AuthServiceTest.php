<?php

namespace App\Tests\Unit\Application\Services;

use App\Application\Services\AuthService;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authenticator\Token\JWTPostAuthenticationToken;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AuthServiceTest extends TestCase
{
    private $tokenManagerMock;
    private $tokenStorageMock;

    /**
     * @throws JWTDecodeFailureException
     */
    public function testExtractUsernameFromTokenWhenTokenExists()
    {

        $tokenPayload = [
            'username' => 'testuser',
            'iat' => '1551515',
            'exp' => '1515141411',
            'roles' => []
        ];

        $this->tokenStorageMock
            ->expects($this->once())
            ->method('getToken')
            ->willReturn($this->createMock(JWTPostAuthenticationToken::class)); // Simulating a token existence

        $this->tokenManagerMock
            ->expects($this->once())
            ->method('decode')
            ->willReturn($tokenPayload);

        $authService = new AuthService($this->tokenManagerMock, $this->tokenStorageMock);

        $username = $authService->extractUsernameFromToken();

        $this->assertEquals('testuser', $username);
    }

    /**
     * @throws JWTDecodeFailureException
     */
    public function testExtractUsernameFromTokenWhenTokenNotExists()
    {
        $this->tokenStorageMock
            ->expects($this->once())
            ->method('getToken')
            ->willReturn(null);

        $this->tokenManagerMock
            ->expects($this->never())
            ->method('decode');

        $authService = new AuthService($this->tokenManagerMock, $this->tokenStorageMock);

        $username = $authService->extractUsernameFromToken();

        $this->assertNull($username);
    }

    public function testExtractUsernameFromTokenThrowsException()
    {
        $this->tokenStorageMock
            ->expects($this->once())
            ->method('getToken')
            ->willReturn($this->createMock(JWTPostAuthenticationToken::class)); // Simulating no token

        $this->tokenManagerMock
            ->expects($this->once())
            ->method('decode')
            ->willThrowException($this->createMock(JWTDecodeFailureException::class));

        $authService = new AuthService($this->tokenManagerMock, $this->tokenStorageMock);

        $this->expectException(JWTDecodeFailureException::class);

        $authService->extractUsernameFromToken();
    }

    protected function setUp(): void
    {
        $this->tokenManagerMock = $this->createMock(JWTTokenManagerInterface::class);
        $this->tokenStorageMock = $this->createMock(TokenStorageInterface::class);
    }
}
