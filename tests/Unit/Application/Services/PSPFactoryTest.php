<?php

namespace App\Tests\Unit\Application\Services;

use App\Application\Services\PSPDeterminationService;
use App\Application\Services\PSPFactory;
use App\Services\PaymentGateway\PinPaymentGateway;
use App\Services\PaymentGateway\StripePaymentGateway;
use InvalidArgumentException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use PHPUnit\Framework\TestCase;

class PSPFactoryTest extends TestCase
{
    /**
     * @throws JWTDecodeFailureException
     */
    public function testPspFactoryWillReturnStripeService()
    {
        $mockDetService = $this->createMock(PSPDeterminationService::class);

        $mockDetService->method('getPSP')
            ->willReturn([
                'Stripe', 'testapikey'
            ]);

        $paymentGatewayService = new PSPFactory($mockDetService);

        $result = $paymentGatewayService->getPaymentGatewayService();

        $this->assertInstanceOf(StripePaymentGateway::class, $result);
    }

    /**
     * @throws JWTDecodeFailureException
     */
    public function testPspFactoryWillReturnPinPaymillService()
    {
        $mockDetService = $this->createMock(PSPDeterminationService::class);

        $mockDetService->method('getPSP')
            ->willReturn([
                'PinPayments', 'testapikey'
            ]);

        $paymentGatewayService = new PSPFactory($mockDetService);

        $result = $paymentGatewayService->getPaymentGatewayService();

        $this->assertInstanceOf(PinPaymentGateway::class, $result);
    }

    /**
     * @throws JWTDecodeFailureException
     */
    public function testPspFactoryWillThrowException()
    {
        $mockDetService = $this->createMock(PSPDeterminationService::class);

        $mockDetService->method('getPSP')
            ->willReturn([
                '', 'testapikey'
            ]);

        $paymentGatewayService = new PSPFactory($mockDetService);

        $this->expectException(InvalidArgumentException::class);

        $paymentGatewayService->getPaymentGatewayService();

    }
}
