<?php

namespace App\Tests\Unit\Application\DTO;

use App\Application\DTO\ChargeCardDetailsDTO;
use App\Application\DTO\Exceptions\InputValidationException;
use App\Application\Interfaces\RequestInterface;
use App\Application\Services\BaseRequestObjectService;
use JsonMapper\JsonMapperInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ChargeCardDetailsDTOTest extends TestCase
{

    private $jsonMapping;
    private $requestStack;
    private $validator;

    /**
     * @throws \Exception
     */
    public function testPopulate()
    {
        $mockObject = (object)[
            'card_number' => 'value1',
            'expiration_date' => 'value2',
            'cvv' => 'value3',
            'cardholder_name' => 'value4',
            'ammount' => 'value5'
        ];

        $this->jsonMapping->method('mapToClass')->willReturn([
            'card_number' => 'mappedValue1',
            'expiration_date' => 'mappedValue2',
            'cvv' => 'mappedValue3',
            'cardholder_name' => 'mappedValue4',
            'ammount' => 'mappedValue5'
        ]);

        $mockRequest = $this->createMock(Request::class);
        $this->requestStack->method('getRequest')->willReturn($mockRequest);
        $mockRequest->method('getContent')->willReturn(json_encode($mockObject));

        $this->validator->method('validate')->willReturn([]);

        $chargeCardDetailsObject = new ChargeCardDetailsDTO
        (
            $this->jsonMapping,
            $this->requestStack,
            $this->validator
        );


        $result = $chargeCardDetailsObject->populate();
        $this->assertInstanceOf(BaseRequestObjectService::class, $result);
        $this->assertEquals([
            'mappedValue1',
            'mappedValue2'
        ], [
            $chargeCardDetailsObject->card_number,
            $chargeCardDetailsObject->expiration_date
        ]);
    }

    /**
     * @throws \Exception
     */
    public function testPopulateWillThrowException()
    {
        $mockObject = (object)[
            'card_number' => '',
            'expiration_date' => 'value2',
            'cvv' => 'value3',
            'cardholder_name' => '',
            'ammount' => 'value5'
        ];

        $this->jsonMapping->method('mapToClass')->willReturn([
            'card_number' => '',
            'expiration_date' => 'mappedValue2',
            'cvv' => 'mappedValue3',
            'cardholder_name' => '',
            'ammount' => 'mappedValue5'
        ]);

        $mockRequest = $this->createMock(Request::class);
        $this->requestStack->method('getRequest')->willReturn($mockRequest);
        $mockRequest->method('getContent')->willReturn(json_encode($mockObject));

        $this->validator->method('validate')->willReturn([
            'card_number cant be null',
            'cardholder_name cant be null'
        ]);

        $chargeCardDetailsObject = new ChargeCardDetailsDTO
        (
            $this->jsonMapping,
            $this->requestStack,
            $this->validator
        );


        $this->expectException(InputValidationException::class);
        $result = $chargeCardDetailsObject->populate();
    }


    protected function setUp(): void
    {
        // Set up mock objects for dependencies
        $this->jsonMapping = $this->createMock(JsonMapperInterface::class);
        $this->requestStack = $this->createMock(RequestInterface::class);
        $this->validator = $this->createMock(ValidatorInterface::class);
    }
}
